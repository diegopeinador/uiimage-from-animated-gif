#import "ViewController.h"
#import "UIImage+animatedGIF.h"

@implementation ViewController
@synthesize urlImageView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"test" withExtension:@"gif"];
    self.urlImageView.image = [UIImage animatedImageWithAnimatedGIFURL:url];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void)viewDidUnload {
    [self setUrlImageView:nil];
    [super viewDidUnload];
}
@end
